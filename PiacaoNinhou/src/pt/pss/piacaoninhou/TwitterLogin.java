package pt.pss.piacaoninhou;

import android.app.Activity;
import android.app.ProgressDialog;
import android.net.Uri;
import android.os.Bundle;
import android.text.Html;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class TwitterLogin extends Activity {
 
	
 
    // Login button
    Button btnLoginTwitter;
    // Update status button
    Button btnUpdateStatus;
    // Logout button
    Button btnLogoutTwitter;
    // EditText for update
    EditText txtUpdate;
    // lbl update
    TextView lblUpdate;
    TextView lblUserName;
 
    // Progress dialog
    ProgressDialog pDialog;
 
    
     TwitterAL tal;
    
 
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.twitter_login);
		
		tal = TwitterAL.getInstance(this);
		
        
        // All UI elements
        btnLoginTwitter = (Button) findViewById(R.id.btnLoginTwitter);
        btnUpdateStatus = (Button) findViewById(R.id.btnUpdateStatus);
        btnLogoutTwitter = (Button) findViewById(R.id.btnLogoutTwitter);
        txtUpdate = (EditText) findViewById(R.id.txtUpdateStatus);
        lblUpdate = (TextView) findViewById(R.id.lblUpdate);
        lblUserName = (TextView) findViewById(R.id.lblUserName);
        
        
        btnLogoutTwitter.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				
				tal.logOut();
				// Show login button
                btnLoginTwitter.setVisibility(View.VISIBLE);
 
                // Hide Update Twitter
                lblUpdate.setVisibility(View.GONE);
                txtUpdate.setVisibility(View.GONE);
                btnUpdateStatus.setVisibility(View.GONE);
                btnLogoutTwitter.setVisibility(View.GONE);
				
			}
		});
        
        btnLoginTwitter.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				tal.login();
				
			}
		});
        
        btnUpdateStatus.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View arg0) {
				if (txtUpdate.getText()!= null && !txtUpdate.getText().toString().equals("")){
					//Toast.makeText(MainActivity.this, txtUpdate.getText().toString(), Toast.LENGTH_LONG).show();
					tal.makeTweet(txtUpdate.getText().toString());
				}
			}
		});
        
        if (!tal.isLoggedIn()) {
            Uri uri = getIntent().getData();
            if (uri != null && uri.toString().startsWith(TwitterAL.TWITTER_CALLBACK_URL)) {
                // oAuth verifier
                String verifier = uri
                        .getQueryParameter(TwitterAL.URL_TWITTER_OAUTH_VERIFIER);
                    
                	tal.requestAccessToken(verifier);
                	
     
                    // Hide login button
                    btnLoginTwitter.setVisibility(View.GONE);
     
                    // Show Update Twitter
                    lblUpdate.setVisibility(View.VISIBLE);
                    txtUpdate.setVisibility(View.VISIBLE);
                    btnUpdateStatus.setVisibility(View.VISIBLE);
                    btnLogoutTwitter.setVisibility(View.VISIBLE);
                     
                    // Displaying in xml ui
                    lblUserName.setText(Html.fromHtml("<b>Welcome " + tal.getUsername() + "</b>"));
            }
        }else {
        	// Hide login button
            btnLoginTwitter.setVisibility(View.GONE);

            // Show Update Twitter
            lblUpdate.setVisibility(View.VISIBLE);
            txtUpdate.setVisibility(View.VISIBLE);
            btnUpdateStatus.setVisibility(View.VISIBLE);
            btnLogoutTwitter.setVisibility(View.VISIBLE);
        }
        
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	
    

}
