package pt.pss.piacaoninhou;

import java.util.concurrent.ExecutionException;

import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.User;
import twitter4j.auth.AccessToken;
import twitter4j.auth.RequestToken;
import twitter4j.conf.Configuration;
import twitter4j.conf.ConfigurationBuilder;
import android.app.AlertDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

public class TwitterAL {
	
	private static TwitterAL instance;
	
	static String TWITTER_CONSUMER_KEY = "aedpA2OoAoaTVDOxPuDg";
    static String TWITTER_CONSUMER_SECRET = "JudOOATqXGV0bizPtF6Kiq2MpRpfFVyBRRxcqkxVA";
 
    // Preference Constants
    static String PREFERENCE_NAME = "twitter_oauth";
    static final String PREF_KEY_OAUTH_TOKEN = "oauth_token";
    static final String PREF_KEY_OAUTH_SECRET = "oauth_token_secret";
    static final String PREF_KEY_TWITTER_LOGIN = "isTwitterLogedIn";
 
    static final String TWITTER_CALLBACK_URL = "piacaoninhou://twitter";
 
    // Twitter oauth urls
    static final String URL_TWITTER_AUTH = "auth_url";
    static final String URL_TWITTER_OAUTH_VERIFIER = "oauth_verifier";
    static final String URL_TWITTER_OAUTH_TOKEN = "oauth_token";
    
    
 // Twitter
    private static Twitter twitter;
    private static RequestToken requestToken;
    private static AccessToken accessToken;
    private static User user; 
     
    // Shared Preferences
    private static SharedPreferences mSharedPreferences;
     
    // Internet Connection detector
    private ConnectionDetector cd;
    
    private String twitErr;
    
    //Context
    private static Context context;
    
    // Alert Dialog Manager
    //AlertDialogManager alert = new AlertDialogManager();
    
    public static TwitterAL getInstance(Context c){
    	context = c;
    	if ( instance == null )
    		instance = new TwitterAL();
    	return instance;
    }
    
    private TwitterAL (){
    	cd = new ConnectionDetector(context.getApplicationContext());
        // Check if Internet present
        if (!cd.isConnectingToInternet()) {
            // Internet Connection is not present
        	//AlertDialogManager.showAlertDialog(context, "Internet Connection Error",
            //        "Please connect to working Internet connection", false);
            // stop executing code by return
            return;
        }
        
        // Shared Preferences
        mSharedPreferences = context.getApplicationContext().getSharedPreferences("PiaNinTwitPref", 0);
		
        
        
    }
    
    private Twitter generateTwitter( ){
    	ConfigurationBuilder builder = new ConfigurationBuilder();
        builder.setOAuthConsumerKey(TWITTER_CONSUMER_KEY);
        builder.setOAuthConsumerSecret(TWITTER_CONSUMER_SECRET);
        Configuration configuration = builder.build();
         
        TwitterFactory factory = new TwitterFactory(configuration);
        return factory.getInstance();
    }
    
    public void login() {
        // Check if already logged in
        if (!isLoggedIn()) {
        	twitter = generateTwitter();
 
            try {
                //requestToken = twitter.getOAuthRequestToken(TWITTER_CALLBACK_URL);
            	AsyncTask<Twitter, Void, RequestToken> reqTokAT = new RetrieveRequestToken().execute(twitter);
            	requestToken = reqTokAT.get();
            	if ( requestToken == null)
            		throw new Exception("Request Token is NULL");
            	
            	//this.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(requestToken.getAuthenticationURL())));
            	//*
            	AlertDialog.Builder alertDialogb = new AlertDialog.Builder(context);
            	 
                // Setting Dialog Title
            	alertDialogb.setTitle(null);
         
            	alertDialogb.setMessage(null);
                TwitterWebView wv = new TwitterWebView(context, requestToken.getAuthenticationURL());
                alertDialogb.setView(wv);
                alertDialogb.show();
            	//*/
               
            	
            } catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (ExecutionException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
        } else {
            // user already logged into twitter
            Toast.makeText(context.getApplicationContext(),
                    "Already Logged into twitter", Toast.LENGTH_LONG).show();
        }
    }
    
    public boolean isLoggedIn(){
    	if ( twitter == null) return false;
    	try {
    		user.getName();
    		return true;
    	}catch(Exception e){
    		return false;
    	}
    	//return mSharedPreferences.getBoolean(PREF_KEY_TWITTER_LOGIN, false);
    }
    
    public void logOut() {
    	if (mSharedPreferences == null)
    	{
    		mSharedPreferences = context.getApplicationContext().getSharedPreferences("PiaNinTwitPref", 0);
    	}
    	Editor e = mSharedPreferences.edit();
		e.putBoolean(PREF_KEY_TWITTER_LOGIN, false);
		e.remove(PREF_KEY_OAUTH_TOKEN);
        e.remove(PREF_KEY_OAUTH_SECRET);
		e.commit();
		if (twitter != null)
		{
			twitter.shutdown();
			twitter = null;
		}
    }
    
    public void requestAccessToken(String verifier){
    	Log.d("TWIT", "Requesting AT");
    	AsyncTask<Object, Void, AccessToken> RAT;
    	RAT = new RetrieveAccessToken().execute(requestToken,verifier);
		try {
			accessToken = RAT.get();
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
			return;
		} catch (ExecutionException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
			return;
		}
    	
    	// Shared Preferences
        Editor e = mSharedPreferences.edit();

        // After getting access token, access token secret
        // store them in application preferences
        e.putString(PREF_KEY_OAUTH_TOKEN, accessToken.getToken());
        e.putString(PREF_KEY_OAUTH_SECRET,
                accessToken.getTokenSecret());
        // Store login status - true
        e.putBoolean(PREF_KEY_TWITTER_LOGIN, true);
        e.commit(); // save changes
        
        Log.d("TWIT", "Getting user info");
        
     // Getting user details from twitter
        // For now i am getting his name only
        long userID = accessToken.getUserId();
        
        AsyncTask<Object, Void, User> RUser;
        RUser = new RetrieveUser().execute(twitter,userID);
		try {
			user = RUser.get();
		} catch (InterruptedException e1) {
			e1.printStackTrace();
			return;
		} catch (ExecutionException e1) {
			e1.printStackTrace();
			return;
		}
    }
    
    public void makeTweet(final String s){
    	if ( isLoggedIn()){
    		TwitterAL.this.twitErr = "";
			Thread t = new Thread(new Runnable() {
				@Override
				public void run() {
					try {
						twitter.updateStatus(s);
						
					} catch (TwitterException e) {
						TwitterAL.this.twitErr = e.getErrorMessage();
					}
				}
			});
			t.start();
			try {
				t.join();
			} catch (InterruptedException e) {
			}
			if ( TwitterAL.this.twitErr.equals("") ){
				Toast.makeText(context, "Tweet sent!!", Toast.LENGTH_LONG).show();
			} else {
				Toast.makeText(context, "Error tweeting: "+TwitterAL.this.twitErr, Toast.LENGTH_LONG).show();
			}
    	}
    }
    
    
    public String getUsername(){
    	if ( isLoggedIn() && user != null)
    		return user.getName();
    	return null;
    	
    }
    
    //
    class RetrieveRequestToken extends AsyncTask<Twitter, Void, RequestToken> {
		@Override
		protected RequestToken doInBackground(Twitter... params) {
			try {
				Twitter t = params[0];
				return t.getOAuthRequestToken(TWITTER_CALLBACK_URL);
			} catch (TwitterException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return null;
		}
    }
    class RetrieveAccessToken extends AsyncTask<Object, Void, AccessToken> {
		@Override
		protected AccessToken doInBackground(Object... params) {
			try {
				RequestToken rt = (RequestToken)params[0];
				String verifier = (String)params[1];
				AccessToken accessToken = twitter.getOAuthAccessToken(
                        rt, verifier);
				return accessToken;
			} catch (TwitterException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return null;
		}
    }
    class RetrieveUser extends AsyncTask<Object, Void, User> {
		@Override
		protected User doInBackground(Object... params) {
			try {
				Twitter t = (Twitter)params[0];
				Long userID = (Long)params[1];
				return t.showUser(userID);
			} catch (TwitterException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return null;
		}
    }
}
