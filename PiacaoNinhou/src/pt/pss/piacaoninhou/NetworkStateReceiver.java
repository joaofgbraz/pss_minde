package pt.pss.piacaoninhou;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.widget.ImageView;

public class NetworkStateReceiver extends BroadcastReceiver {

	@Override
	public void onReceive(Context context, final Intent intent) {
		 if (intent.getExtras() != null) {
	            final ConnectivityManager connectivityManager = (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
	            final NetworkInfo ni = connectivityManager.getActiveNetworkInfo();
	            
	            if (MainActivity.self == null)
	            {
	            	return;
	            }
	            
	            final ImageView ivOnlineStatus = MainActivity.self.networkStatus;

	            
	            if (ni != null && ni.isConnectedOrConnecting()) {
	            	ivOnlineStatus.setImageResource(R.drawable.green_dot);
	            } else  if (intent.getBooleanExtra(ConnectivityManager.EXTRA_NO_CONNECTIVITY, Boolean.FALSE)) {
	            	ivOnlineStatus.setImageResource(R.drawable.red_dot);
	            }
	            MainActivity.self.SetLanguages();
	            
	        }

	}

}
