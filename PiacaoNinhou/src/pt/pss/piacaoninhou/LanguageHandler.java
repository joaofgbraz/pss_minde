package pt.pss.piacaoninhou;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.StringTokenizer;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public class LanguageHandler {

	private static LanguageHandler instance;

	private static HashMap<String, String> languages;
	private List<String> supported;

	private static Boolean isOnline = true;

	private LanguageHandler(Context ctx) {

		supported = new ArrayList<String>();

		if (isOnline) {
			languages = new HashMap<String, String>();
			final String urlS = "http://www.poio.eu/api/languages";
			Thread t = new Thread(new Runnable() {
				@Override
				public void run() {
					StringBuilder builder = new StringBuilder();
					HttpClient client = new DefaultHttpClient();
					HttpGet httpGet = new HttpGet(urlS);
					httpGet.addHeader("X-Poio-Android-IPL",
							"CuYRyGrY7yr72TRxPo92tqP");
					try {
						HttpResponse response = client.execute(httpGet);
						StatusLine statusLine = response.getStatusLine();
						int statusCode = statusLine.getStatusCode();
						if (statusCode == 200) {
							HttpEntity entity = response.getEntity();
							InputStream content = entity.getContent();

							BufferedReader reader = new BufferedReader(
									new InputStreamReader(content));
							String line;
							while ((line = reader.readLine()) != null) {
								builder.append(line);
							}
						} else {

						}
					} catch (ClientProtocolException e) {
						e.printStackTrace();
					} catch (IOException e) {
						e.printStackTrace();
					}

					String result = builder.toString();

					JSONArray jsonResultArr;
					try {
						jsonResultArr = new JSONArray(result);
						for (int i = 0; i < jsonResultArr.length(); i++) {
							String code = jsonResultArr.getString(i);
							supported.add(code);
						}
					} catch (JSONException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					if (supported.size() <= 0) {
						supported.add("bar");
					}
				}
			});
			t.start();
		} else {
			if (ctx != null) {
				SharedPreferences sp = ctx.getSharedPreferences(
						"PiaNinTwitPref", 0);
				String langBD = sp.getString("languagesBD", "bar");
				StringTokenizer tokens = new StringTokenizer(langBD, "|");

				String l = "";
				int c = tokens.countTokens();
				for (int i = 0; i < c; i++) {
					l = tokens.nextToken();
					supported.add(l);
				}
			}
		}
	}

	private static LanguageHandler getInstance() {
		if (instance == null) {
			instance = new LanguageHandler(MainActivity.self.getAppContext());
		}
		return instance;
	}

	public static void setLanguages(HashMap<String, String> languages,
			Boolean isOn, Context ctx) {

		isOnline = isOn;
		if (isOnline) {
			LanguageHandler.instance = null;
			LanguageHandler.getInstance();
			LanguageHandler.languages = languages;
		} else {
			LanguageHandler.languages = languages;
			LanguageHandler.instance = new LanguageHandler(ctx);
		}

	}

	public static String getLanguageName(String code) {
		if (LanguageHandler.instance == null)
			LanguageHandler.getInstance();
		String language = LanguageHandler.instance.languages.get(code);
		return language;
	}

	public static List<String> getSupportedLanguages() {
		if (LanguageHandler.instance == null)
			LanguageHandler.getInstance();
		return instance.supported;
	}

}
