package pt.pss.piacaoninhou;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.ArrayList;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.provider.MediaStore.Images;
import android.provider.MediaStore.Images.Media;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

@TargetApi(Build.VERSION_CODES.FROYO)
public class EmailActivity extends Activity {

	int photoState = 0;
	int audioState = 0;
	Uri uriFoto;
	Uri uriAudio;
	String fileNameFoto;
	String fileNameAudio;
	
	ImageButton btnFotoRemove = null;
	ImageButton btnAudioRemove = null;

	
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.email);
        
        btnFotoRemove = (ImageButton)findViewById(R.id.buttonFotoRemove);
		btnAudioRemove = (ImageButton)findViewById(R.id.buttonAudioRemove);
		
		btnFotoRemove.setVisibility(View.GONE);
		btnAudioRemove.setVisibility(View.GONE);
		
        if (savedInstanceState != null) {
        	photoState = savedInstanceState.getInt("photo_state");
            audioState = savedInstanceState.getInt("audio_state");

	        if (photoState == 1)
	        {
	        	uriFoto = Uri.parse(savedInstanceState.getString("photo_uri"));
	        	fileNameFoto = savedInstanceState.getString("photo_filename");
	        	
	        	final ImageView ivFoto = (ImageView) findViewById(R.id.imageViewFoto);
				final TextView tvFoto = (TextView) findViewById(R.id.textViewFotoFileName);
				
				ivFoto.setImageResource(R.drawable.image_loaded);
				tvFoto.setText(fileNameFoto);
				
				btnFotoRemove.setVisibility(View.VISIBLE);
	        }
	        
	        if (audioState == 1)
	        {
	        	uriAudio = Uri.parse(savedInstanceState.getString("audio_uri"));
	        	fileNameAudio = savedInstanceState.getString("audio_filename");
	        	
	        	final ImageView ivAudio = (ImageView) findViewById(R.id.imageViewAudio);
				final TextView tvAudio = (TextView) findViewById(R.id.textViewAudioFileName);
				
				ivAudio.setImageResource(R.drawable.audio_loaded);
				tvAudio.setText(fileNameAudio);
				
				btnAudioRemove.setVisibility(View.VISIBLE);
	        }
        } 
        
        Intent i = getIntent();
        final String message = i.getStringExtra("message");
        final String lang = i.getStringExtra("lang");
        
        TextView tv = (TextView) findViewById(R.id.textViewEmail);
        tv.setText(message);
        
        final Button btnEmailSend = (Button)findViewById(R.id.buttonEmailSend);
		btnEmailSend.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				
				
		        ArrayList<Uri> attachments = new ArrayList<Uri>();
		        if (photoState == 1){
		        	File filePic = new File(uriFoto.toString());
			        Uri pathPic = Uri.fromFile(filePic);
		        	attachments.add(pathPic);
		        }
		        if (audioState == 1){
		        	File fileAud = new File(uriAudio.toString());
			        Uri pathAud = Uri.fromFile(fileAud);
		        	attachments.add(pathAud);
		        }
				Intent email = new Intent(Intent.ACTION_SEND_MULTIPLE);
				email.putExtra(Intent.EXTRA_TEXT, message);
				email.putExtra(Intent.EXTRA_SUBJECT, lang);
				email.putParcelableArrayListExtra(Intent.EXTRA_STREAM, attachments);
				email.setType("message/rfc822");
				startActivity(Intent.createChooser(email, "Choose an E-Mail client:"));
				
			}
		});
		
		btnFotoRemove.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				final ImageView ivFoto = (ImageView) findViewById(R.id.imageViewFoto);
				final TextView tvFoto = (TextView) findViewById(R.id.textViewFotoFileName);
				
				uriFoto = null;
				fileNameFoto = null;
				photoState = 0;
				
				ivFoto.setImageResource(R.drawable.no_image_loaded);
				tvFoto.setText("No Photo Loaded");
				btnFotoRemove.setVisibility(View.GONE);
			}
		});
		
		btnAudioRemove.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				final ImageView ivAudio = (ImageView) findViewById(R.id.imageViewAudio);
				final TextView tvAudio = (TextView) findViewById(R.id.textViewAudioFileName);
				
				uriAudio = null;
				fileNameAudio = null;
				audioState = 0;
				
				ivAudio.setImageResource(R.drawable.no_audio_loaded);
				tvAudio.setText( "No Audio Loaded");

				btnAudioRemove.setVisibility(View.GONE);
				
			}
		});
		
		final AlertDialog.Builder alertFoto = new AlertDialog.Builder(EmailActivity.this);
		
		alertFoto.setTitle("Photo");
    	alertFoto.setMessage("What do you want to do?");
    	
		alertFoto.setPositiveButton("Take photo", new DialogInterface.OnClickListener() {
    	public void onClick(DialogInterface dialog, int whichButton) {
    		
			ContentValues values = new ContentValues();
			values.put(MediaStore.Images.Media.TITLE, "capture.jpg");
			
			Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
		    startActivityForResult(takePictureIntent, 1);
    	  }
    	});

    	alertFoto.setNegativeButton("Choose image", new DialogInterface.OnClickListener() {
    	  public void onClick(DialogInterface dialog, int whichButton) {
    		  Intent fotoIntentBak = new Intent(Intent.ACTION_GET_CONTENT); 
    		  fotoIntentBak.setType("image/*"); 
    		  fotoIntentBak.addCategory(Intent.CATEGORY_OPENABLE);
    		  startActivityForResult(Intent.createChooser(fotoIntentBak, "Select a Photo:"), 3);
    	  }
    	});
    	
    	
    	final AlertDialog.Builder alertAudio = new AlertDialog.Builder(EmailActivity.this);
		
		alertAudio.setTitle("Audio");
    	alertAudio.setMessage("What do you want to do?");
    	
		alertAudio.setPositiveButton("Record audio", new DialogInterface.OnClickListener() {
    	public void onClick(DialogInterface dialog, int whichButton) {
    		
    		Intent audioIntent = new Intent(MediaStore.Audio.Media.RECORD_SOUND_ACTION);
			startActivityForResult(audioIntent, 2);
		  }
    	});

    	alertAudio.setNegativeButton("Choose audio", new DialogInterface.OnClickListener() {
    	  public void onClick(DialogInterface dialog, int whichButton) {
    		  
    		  Intent audioIntentBak = new Intent(Intent.ACTION_GET_CONTENT);
    		  audioIntentBak.setType("audio/*"); 
    		  audioIntentBak.addCategory(Intent.CATEGORY_OPENABLE);
    		  startActivityForResult(Intent.createChooser(audioIntentBak, "Select an Audio File:"), 2);
    	  }
    	});
		
		final LinearLayout llFoto = (LinearLayout) findViewById(R.id.LinearLayoutFoto);
		
		llFoto.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				
			    Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
			    if (takePictureIntent.resolveActivity(getPackageManager()) != null)
			    {
			    	alertFoto.show();
			    }
			    
			    else
			    {
			    	Intent fotoIntentBak = new Intent(Intent.ACTION_GET_CONTENT); 
				    fotoIntentBak.setType("image/*"); 
				    fotoIntentBak.addCategory(Intent.CATEGORY_OPENABLE);
				    startActivityForResult(Intent.createChooser(fotoIntentBak, "Select a Photo:"), 3);
			    }
			}
		});
		
		final LinearLayout llAudio = (LinearLayout) findViewById(R.id.LinearLayoutAudio);
		
		llAudio.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				
				Intent audioIntent = new Intent(MediaStore.Audio.Media.RECORD_SOUND_ACTION);
				if (audioIntent.resolveActivity(getPackageManager()) != null) {
					alertAudio.show();
				}
				
				else
				{
					Intent audioIntentBak = new Intent(Intent.ACTION_GET_CONTENT);
					audioIntentBak.setType("audio/*"); 
				    audioIntentBak.addCategory(Intent.CATEGORY_OPENABLE);
				    startActivityForResult(Intent.createChooser(audioIntentBak, "Select an Audio File:"), 2);
				}
			}
		});
		
    }
	
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {

		if (resultCode == RESULT_OK && data != null) {
			
			Uri uri = null;
			if (requestCode == 1)
			{
				Bitmap photo = (Bitmap) data.getExtras().get("data");
				uri = getImageUri(getApplicationContext(), photo);
			}
			
			else if (requestCode == 2 || requestCode == 3)
			{
				uri = data.getData();
			}
			
			String fileName = "";
			 
			Cursor cursor = EmailActivity.this.getContentResolver().query(uri, null, null, null, null);
			if ( cursor == null ) return;
			cursor.moveToFirst();
	        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);//Instead of "MediaStore.Images.Media.DATA" can be used "_data"
	        uri = Uri.parse(cursor.getString(column_index));
	        fileName = uri.getLastPathSegment().toString();

	        if (requestCode == 1 || requestCode == 3)
			{
				final ImageView ivFoto = (ImageView) findViewById(R.id.imageViewFoto);
				final TextView tvFoto = (TextView) findViewById(R.id.textViewFotoFileName);
				
				uriFoto = uri;
				fileNameFoto = fileName;
				photoState = 1;
				
				ivFoto.setImageResource(R.drawable.image_loaded);
				tvFoto.setText(fileNameFoto);
				
				btnFotoRemove.setVisibility(View.VISIBLE);
				
			}
	        
	        else if (requestCode == 2)
	        {
				final ImageView ivAudio = (ImageView) findViewById(R.id.imageViewAudio);
				final TextView tvAudio = (TextView) findViewById(R.id.textViewAudioFileName);
				
				uriAudio = uri;
				fileNameAudio = fileName;
				audioState = 1;
				
				ivAudio.setImageResource(R.drawable.audio_loaded);
				tvAudio.setText(fileNameAudio);
	
				btnAudioRemove.setVisibility(View.VISIBLE);
			}
		
		}

	    super.onActivityResult(requestCode, resultCode, data);
	}

	public Uri getImageUri(Context inContext, Bitmap inImage) {
	    ByteArrayOutputStream bytes = new ByteArrayOutputStream();
	    inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
	    String path = Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
	    return Uri.parse(path);
	}
	
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        if (photoState == 1)
        {
        	outState.putInt("photo_state", photoState);
        	outState.putString("photo_uri", uriFoto.toString());
        	outState.putString("photo_filename", fileNameFoto);
        }
        
        if (audioState == 1)
        {
        	outState.putInt("audio_state", audioState);
        	outState.putString("audio_uri", uriAudio.toString());
        	outState.putString("audio_filename", fileNameAudio);
        }
        
    }

}
