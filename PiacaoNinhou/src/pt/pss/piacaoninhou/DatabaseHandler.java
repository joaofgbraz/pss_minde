package pt.pss.piacaoninhou;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Environment;

public class DatabaseHandler  {

	private static DatabaseHandler self = null;
	SQLiteDatabase db = null;
	
	private DatabaseHandler(){
		connectToDatabase(MainActivity.LANGUAGE_CODE);
	}
	
	private void connectToDatabase(String code){
		try {
			db = SQLiteDatabase.openDatabase(
					Environment.getExternalStorageDirectory().getPath() + "/PiacaoNinhou/databases/" + code +".sqlite"
					, null
					, 0);
		} catch(Exception e){
			db = null;
		}
	}
	
	public String[] getWordPrediction(String part){
		try{
			Cursor c = db.rawQuery("SELECT word FROM _1_gram WHERE word like '"+part+"%' order by count desc LIMIT 6;", null);
			String[]  results = new String[6];
			if ( c.moveToFirst() ){
				for(int i =0; i<6; i++){
					results[i]=c.getString(0);
					if( !c.moveToNext()) break;
				}
			}
			c.close();
			return results;
		} catch ( Exception e ){
			return new String[0];
		}
	}
	
	public static DatabaseHandler getInstance(){
		if ( self == null)
			self = new DatabaseHandler();
		return self;
	}
	public static void deleteInstance(){
		if ( self.db != null) self.db.close();
		self = null;
	}

}
