package pt.pss.piacaoninhou;

import java.util.ArrayList;
import java.util.List;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.SharedPreferences.Editor;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

@TargetApi(Build.VERSION_CODES.FROYO)
public class RCSettingsActivity extends Activity {

	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.settings);
        Spinner sp = (Spinner)findViewById(R.id.spinner1);
        final List <String> languages = LanguageHandler.getSupportedLanguages();
        List <String> languagesName = new ArrayList<String>();
        for( String langC : languages){
        	languagesName.add(LanguageHandler.getLanguageName(langC));
        }
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, languagesName);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sp.setAdapter(adapter);
        
        sp.setOnItemSelectedListener(new OnItemSelectedListener() {
	        @Override
	        public void onItemSelected(AdapterView<?> arg0, View arg1,
	        		int arg2, long arg3) {
	        	Editor e = RCSettingsActivity.this.getApplicationContext().getSharedPreferences("PiaNinTwitPref", 0).edit();
				e.putString("language", languages.get(arg2));
				e.commit();
				
				MainActivity.LANGUAGE_CODE = (String) languages.get(arg2);
				DatabaseHandler.deleteInstance();
	        	MainActivity.self.dbh = DatabaseHandler.getInstance();
	        }

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub
				
			}
		});
        try {
        	 sp.setSelection(languages.indexOf(MainActivity.LANGUAGE_CODE));   
		} catch (Exception e) {
			e.printStackTrace();
		}
       
        /*
        http://www.poio.eu/api/languages
        */
    }

}
