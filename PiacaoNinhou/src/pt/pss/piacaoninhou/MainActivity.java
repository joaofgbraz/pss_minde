package pt.pss.piacaoninhou;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Field;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.StringTokenizer;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;

import twitter4j.internal.logging.Logger;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.LabeledIntent;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MenuItem.OnMenuItemClickListener;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewConfiguration;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity {

	protected static final int DOWNLOAD_BUFFER_SIZE = 1024;

	public static String LANGUAGE_CODE = "bar";

	public static MainActivity self;
	public ImageView networkStatus;

	public DatabaseHandler dbh;

	private Thread t = null;
	private Thread tt = null;
	Button btnPrev1 = null;
	Button btnPrev2 = null;
	Button btnPrev3 = null;
	Button btnPrev4 = null;
	Button btnPrev5 = null;
	Button btnPrev6 = null;

	ImageButton btnFotoRemove = null;
	ImageButton btnAudioRemove = null;

	ProgressDialog initRingProgressDialog;
	
	private Handler handler = new Handler();
	private Runnable calculateRunnable;

	boolean buttonsWaiting = false;

	ArrayList<Button> buttons;

	Uri uriFoto;
	Uri uriAudio;
	String fileNameFoto;
	String fileNameAudio;

	TwitterAL tal;

	static Context ctx;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getOverflowMenu();
		setContentView(R.layout.activity_main);

		ctx = getApplicationContext();
		SharedPreferences sp = this.getApplicationContext()
				.getSharedPreferences("PiaNinTwitPref", 0);
		LANGUAGE_CODE = sp.getString("language", "bar");
		
		self = this;
	
		final TextView txtLing = (TextView) MainActivity.self.findViewById(R.id.txtLinguagem);
		txtLing.setText("-");
		TextView txtLingName = (TextView) findViewById(R.id.txtLinguagemText);
		OnClickListener ocl = new OnClickListener() {
			@Override
			public void onClick(View v) {
				MainActivity.this.startActivity(new Intent(MainActivity.this, RCSettingsActivity.class));
			}
		};
		
		txtLing.setOnClickListener(ocl);
		txtLingName.setOnClickListener(ocl);
		
		networkStatus = (ImageView) findViewById(R.id.networkStatus);

		tal = TwitterAL.getInstance(this);

		btnPrev1 = (Button) findViewById(R.id.buttonPrev1);
		btnPrev2 = (Button) findViewById(R.id.buttonPrev2);
		btnPrev3 = (Button) findViewById(R.id.buttonPrev3);
		btnPrev4 = (Button) findViewById(R.id.buttonPrev4);
		btnPrev5 = (Button) findViewById(R.id.buttonPrev5);
		btnPrev6 = (Button) findViewById(R.id.buttonPrev6);

		this.buttons = new ArrayList<Button>(6);
		buttons.add(btnPrev1);
		buttons.add(btnPrev2);
		buttons.add(btnPrev3);
		buttons.add(btnPrev4);
		buttons.add(btnPrev5);
		buttons.add(btnPrev6);

		final EditText tv = (EditText) findViewById(R.id.editTextMain);

		tv.setText(sp.getString("message", ""));
		final ImageView ivOnlineStatus = MainActivity.self.networkStatus;
		if (internetConnection()) {
			ivOnlineStatus.setImageResource(R.drawable.green_dot);
		}

		else {
			ivOnlineStatus.setImageResource(R.drawable.red_dot);
		}

		final Button btnTwitter = (Button) findViewById(R.id.buttonTwitter);

		btnTwitter.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				if (internetConnection()) {
					if (!tal.isLoggedIn()) {
						tal.login();
					} else {
						AlertDialog.Builder adb = new AlertDialog.Builder(
								MainActivity.this);
						adb.setTitle("Send by Twitter");
						adb.setMessage("Message to tweet:");
						final EditText et = new EditText(MainActivity.this);
						et.setText(tv.getText().toString()
								+ " #"
								+ LanguageHandler
										.getLanguageName(LANGUAGE_CODE));
						adb.setView(et);
						adb.setPositiveButton("Send",
								new DialogInterface.OnClickListener() {
									@Override
									public void onClick(DialogInterface dialog,
											int which) {
										tal.makeTweet(et.getText().toString());
									}
								});
						adb.setNegativeButton("Cancel",
								new DialogInterface.OnClickListener() {
									@Override
									public void onClick(DialogInterface dialog,
											int which) {
										// Nothing
									}
								});
						adb.show();
					}
				}

				else {
					Toast.makeText(MainActivity.this, "No internet connection",
							Toast.LENGTH_LONG).show();
				}
			}

		});

		if (!tal.isLoggedIn()) {
			Log.d("TWIT", "Is NOT logged in");
			Uri uri = getIntent().getData();

			if (uri != null
					&& uri.toString()
							.startsWith(TwitterAL.TWITTER_CALLBACK_URL)) {
				Log.d("TWIT", "Has a callback");
				// oAuth verifier
				String verifier = uri
						.getQueryParameter(TwitterAL.URL_TWITTER_OAUTH_VERIFIER);
				if (verifier != null) {
					tal.requestAccessToken(verifier);
					if (tal.isLoggedIn()) {
						btnTwitter.performClick();
					}
				}
			}
		} else {
			Log.d("TWIT", "Is logged in");
		}

		final View mainView = findViewById(R.id.RelativeLayoutMain);
		mainView.getViewTreeObserver().addOnGlobalLayoutListener(
				new OnGlobalLayoutListener() {
					@Override
					public void onGlobalLayout() {

						float rat = 0.0f;

						int orientation = getResources().getConfiguration().orientation;

						if (orientation == 1) {
							rat = (float) mainView.getWidth()
									/ (float) mainView.getHeight();
						}

						else if (orientation == 2) {
							rat = (float) mainView.getHeight()
									/ (float) mainView.getWidth();
						}

						if ((orientation == 1 && rat > 1.65f)
								|| (orientation == 2 && rat < 0.4f)) {
							LinearLayout lLang = (LinearLayout) findViewById(R.id.LinearLayoutLang);
							lLang.setVisibility(View.GONE);

							LinearLayout lPrev2 = (LinearLayout) findViewById(R.id.LinearLayoutPrevisao2);
							lPrev2.setVisibility(View.GONE);
						} else {
							LinearLayout lLang = (LinearLayout) findViewById(R.id.LinearLayoutLang);
							lLang.setVisibility(View.VISIBLE);

							LinearLayout lPrev2 = (LinearLayout) findViewById(R.id.LinearLayoutPrevisao2);
							lPrev2.setVisibility(View.VISIBLE);
						}
					}
				});

		for (final Button btn : buttons) {
			btn.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View arg0) {
					try {
						if (buttonsWaiting)
							return;
						String t = tv.getText().toString();
						int latest = t.lastIndexOf(" ") > t.lastIndexOf("\n") ? t
								.lastIndexOf(" ") : t.lastIndexOf("\n");
	
						String temp = t.substring(0, latest + 1);
						char sp = ' ';
						if (temp.length() >= 2) {
							sp = t.charAt(temp.length() - 2);
						} else if (tv.length() == 0) {
							sp = '.';
						}
						String btnText = btn.getText().toString();
						if (sp == '.') {
							btnText = btnText.replaceFirst(String.valueOf(btnText
									.charAt(0)), String.valueOf(Character
									.toUpperCase(btnText.charAt(0))));
						}
						t = t.substring(0, latest + 1) + btnText + " ";
						tv.setText(t);
						tv.setSelection(t.length());
					} catch(Exception e){
						
					}
				}
			});
		}

		final Button btnEmail = (Button) findViewById(R.id.buttonEmail);

		btnEmail.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				Intent i = new Intent(MainActivity.this, EmailActivity.class);
				i.putExtra("message", tv.getText().toString());
				i.putExtra(
						"lang",
						"Text written in "
								+ LanguageHandler
										.getLanguageName(LANGUAGE_CODE));
				startActivity(i);
			}
		});

		final Button btnSMS = (Button) findViewById(R.id.buttonSMS);
		if (this.getPackageManager().hasSystemFeature(
				PackageManager.FEATURE_TELEPHONY)) {
			btnSMS.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View arg0) {
					Intent intent = new Intent(Intent.ACTION_VIEW);
					intent.setData(Uri.parse("sms:"));
					intent.putExtra("sms_body", tv.getText().toString());
					startActivity(Intent.createChooser(intent,
							"Choose an SMS client:"));

				}
			});
		} else {
			btnSMS.setBackgroundResource(R.layout.disabled_buttons2);
		}

		Logger.getLogger(MainActivity.class).debug(
				"preparing to set the text listener");

		tv.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(final CharSequence arg0, final int start,
					final int count, final int after) {

				MainActivity.this.calculateRunnable = new Runnable() {

					@Override
					public void run() {
						predictText(tv, arg0, start, count, after);
					}
				};

				handler.removeCallbacksAndMessages(null);
				handler.postDelayed(MainActivity.this.calculateRunnable, 500);

			}

			@Override
			public void beforeTextChanged(CharSequence arg0, int start,
					int count, int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable arg0) {
				// TODO Auto-generated method stub
				try {
					int length = arg0.length();
					if (length >= 2) {
						char ch2 = arg0.charAt(length - 2);
						char ch = arg0.charAt(length - 1);
						if (ch == '.' && ch2 == ' ') {
							String text = arg0.toString().replace(" .", ". ");
							tv.setText(text);
							tv.setSelection(text.length());
						}
					}

				} catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
				}
			}
		});

		//  
		initRingProgressDialog=
		 ProgressDialog.show(MainActivity.this, "Please wait ...",
		 "Loading ...", true);
			MainActivity.self = this;
			Thread temp = new Thread( new Runnable() {
				
				@Override
				public void run() {
					MainActivity.self.SetLanguages();
					handler.post(new Runnable() {
		                public void run() {
		                	txtLing.setText(" "
		            				+ LanguageHandler.getLanguageName(MainActivity.LANGUAGE_CODE));
		                }
		            });
					if( initRingProgressDialog != null && initRingProgressDialog.isShowing()){
						initRingProgressDialog.dismiss();
						initRingProgressDialog = null;
					}
				}
			});
			temp.start();
		
		dbh = DatabaseHandler.getInstance();
		
	}

	  public static Context getAppContext() {
	        return ctx;
	    } 
	public void SetLanguages() {
		try {

			InputStream ims = getAssets().open("lang.csv");
			BufferedReader br = new BufferedReader(new InputStreamReader(ims));
			String buff;
			HashMap<String, String> languages;
			languages = new HashMap<String, String>();
			StringTokenizer tokens = new StringTokenizer(LanguagesBD(), "|");
			
			int c = tokens.countTokens();
			String[] langBD = new String[c];
			for (int i= 0; i< c ; i++)
			{
				langBD[i] = tokens.nextToken();
				
			}
			while ((buff = br.readLine()) != null) {

				String[] split = buff.split(";");
				String code = split[0];
				String name = split[1];
				if (internetConnection()) {
					languages.put(code, name);
				} else {							
					for(String l : langBD)
					{					
						if (l.equals(code))
							languages.put(code, name);
					}

				}
			}
			LanguageHandler.setLanguages(languages, internetConnection(),getApplicationContext());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	protected void predictText(EditText tv, CharSequence arg0, int start,
			int count, int after) {
		String preparedText = "";
		try {
			preparedText = URLEncoder.encode(arg0 + "", "utf-8");
		} catch (UnsupportedEncodingException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		buttonsWaiting = true;
		for (Button b : buttons)
			b.setEnabled(false);
		// gle = irish
		// bar = bavarian
		final String urlS = "http://www.poio.eu/api/prediction?iso="
				+ LANGUAGE_CODE + "&text=" + preparedText;
		if (t != null) {
			t.interrupt();
			t = null;
		}
		final String lastWord = arg0.toString().substring(
				arg0.toString().lastIndexOf(" ") + 1);
		// dbh.getWordPrediction(preparedText);
		if (!internetConnection()) {
			t = new Thread(new Runnable() {
				@Override
				public void run() {
					String[] resultados = dbh.getWordPrediction(lastWord);

					int i = 0;
					for (final String result : resultados) {

						final Button btn = buttons.get(i++);
						btn.post(new Runnable() {

							@Override
							public void run() {
								btn.setText(result);
							}
						});
					}

					buttonsWaiting = false;
					for (final Button b : buttons) {
						b.post(new Runnable() {

							@Override
							public void run() {
								b.setEnabled(true);
							}
						});
					}

				}
			});
		} else {

			t = new Thread(new Runnable() {
				@Override
				public void run() {
					StringBuilder builder = new StringBuilder();
					HttpClient client = new DefaultHttpClient();
					HttpGet httpGet = new HttpGet(urlS);
					httpGet.addHeader("X-Poio-Android-IPL",
							"CuYRyGrY7yr72TRxPo92tqP");
					try {
						HttpResponse response = client.execute(httpGet);
						StatusLine statusLine = response.getStatusLine();
						int statusCode = statusLine.getStatusCode();
						if (statusCode == 200) {
							HttpEntity entity = response.getEntity();
							InputStream content = entity.getContent();

							BufferedReader reader = new BufferedReader(
									new InputStreamReader(content));
							String line;
							while ((line = reader.readLine()) != null) {
								builder.append(line);
							}
						} else {

						}
					} catch (ClientProtocolException e) {
						e.printStackTrace();
					} catch (IOException e) {
						e.printStackTrace();
					}

					String result = builder.toString();
					JSONArray jsonResultArr;
					try {
						jsonResultArr = new JSONArray(result);

						for (int i = 0; i < jsonResultArr.length(); i++) {
							final String jResult = jsonResultArr.getString(i);

							final Button btn = buttons.get(i);
							btn.post(new Runnable() {

								@Override
								public void run() {
									btn.setText(jResult);
								}
							});
						}

					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

					buttonsWaiting = false;
					for (final Button b : buttons) {
						b.post(new Runnable() {

							@Override
							public void run() {
								b.setEnabled(true);
							}
						});
					}
				}
			});
		}
		t.start();

	}

	private void getOverflowMenu() {
		try {
			ViewConfiguration config = ViewConfiguration.get(MainActivity.this);
			Field menuKeyField = ViewConfiguration.class
					.getDeclaredField("sHasPermanentMenuKey");
			if (menuKeyField != null) {
				menuKeyField.setAccessible(true);
				menuKeyField.setBoolean(config, false);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@Override
	protected void onResume() {
		super.onResume();
		TextView txtLing = (TextView) findViewById(R.id.txtLinguagem);
		txtLing.setText(" "
				+ LanguageHandler.getLanguageName(MainActivity.LANGUAGE_CODE));

		final ImageView ivOnlineStatus = MainActivity.self.networkStatus;
		if (internetConnection()) {
			ivOnlineStatus.setImageResource(R.drawable.green_dot);

		}

		else {
			ivOnlineStatus.setImageResource(R.drawable.red_dot);
		}

	}

	@Override
	protected void onPause() {
		super.onPause();
		Editor e = this.getApplicationContext()
				.getSharedPreferences("PiaNinTwitPref", 0).edit();
		final EditText tv = (EditText) findViewById(R.id.editTextMain);
		e.putString("message", tv.getText().toString());
		e.commit();
		
		if ( initRingProgressDialog != null && initRingProgressDialog.isShowing() ){
			initRingProgressDialog.dismiss();
		}
		initRingProgressDialog = null;

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		menu.getItem(0).setOnMenuItemClickListener(
				new OnMenuItemClickListener() {

					@Override
					public boolean onMenuItemClick(MenuItem item) {
						Intent i = new Intent(MainActivity.this,
								RCSettingsActivity.class);
						startActivity(i);
						return false;
					}
				});
		menu.getItem(1).setOnMenuItemClickListener(
				new OnMenuItemClickListener() {

					@Override
					public boolean onMenuItemClick(MenuItem item) {
						TwitterAL.getInstance(MainActivity.this).logOut();
						return false;
					}
				});
		menu.getItem(2).setOnMenuItemClickListener(
				new OnMenuItemClickListener() {

					@Override
					public boolean onMenuItemClick(MenuItem item) {
						AtualizarBD();
						return false;
					}
				});
		return true;
	}

	private String LanguagesBD() {
		SharedPreferences sp = this.getApplicationContext()
				.getSharedPreferences("PiaNinTwitPref", 0);
		return sp.getString("languagesBD", "bar");
	}

	private void AtualizarBD() {
		final ProgressDialog ringProgressDialog = new ProgressDialog(this);
		//final ProgressDialog ringProgressDialog =
    	//ProgressDialog.show(MainActivity.this, "Please wait ...",
    	//	"Downloading Database ...", false);
		ringProgressDialog.setTitle("Downloading...");
		ringProgressDialog.setIndeterminate(false);
		ringProgressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
		ringProgressDialog.setProgress(0);
		final String dir = Environment.getExternalStorageDirectory().getPath()
				+ "/PiacaoNinhou/databases/";
		File f = new File(dir);
		if (!f.exists())
			f.mkdirs();

		final String uri = "http://s3.amazonaws.com/poiocorpus/prediction/"
				+ LANGUAGE_CODE + ".sqlite.zip";

		if (tt != null) {
			tt.interrupt();
			tt = null;
		}
		ringProgressDialog.show();
		
		tt = new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					
					URL url = new URL(uri);
					File fileZ = new File(dir, LANGUAGE_CODE + ".zip");
					URLConnection conn;
					int fileSize;
					Message m;
					//org.apache.commons.io.FileUtils.copyURLToFile(url, fileZ);
			        conn = url.openConnection();
			        conn.setUseCaches(false);
			        fileSize = conn.getContentLength();
			        
			        if ( fileSize <= 0){
			        	ringProgressDialog.dismiss();
			        	MainActivity.this.handler.post(new Runnable() {
							@Override
							public void run() {
								Toast.makeText(MainActivity.this, "Language not supported or no Internet Connection.", Toast.LENGTH_LONG).show();
							}
						});
			        	return;
			        }
			        
					ringProgressDialog.setMax(fileSize/1024);
			        
			        BufferedInputStream inStream = new BufferedInputStream(conn.getInputStream());
			        FileOutputStream fileStream = new FileOutputStream(fileZ);
			        BufferedOutputStream outStream = new BufferedOutputStream(fileStream, DOWNLOAD_BUFFER_SIZE);
			        
			        byte[] data = new byte[DOWNLOAD_BUFFER_SIZE];
			        int bytesRead = 0, totalRead = 0;
			        
			        while((bytesRead = inStream.read(data, 0, data.length)) >= 0)
			        {
			            outStream.write(data, 0, bytesRead);
			 
			            // update progress bar
			            totalRead += bytesRead;
			            int totalReadInKB = totalRead / 1024;
			            ringProgressDialog.setProgress(totalReadInKB);
			        }
			 
			        outStream.close();
			        fileStream.close();
			        inStream.close();
			        
			        
					unzip(dir);

					if (ringProgressDialog.isShowing())
						ringProgressDialog.dismiss();
				} catch (Exception e) {
					ringProgressDialog.dismiss();
		        	MainActivity.this.handler.post(new Runnable() {
						@Override
						public void run() {
							Toast.makeText(MainActivity.this, "Error gathering language file.", Toast.LENGTH_LONG).show();
						}
					});
				}
			}
		});
		tt.start();	
		String langBD = LanguagesBD();

		
		StringTokenizer tokens = new StringTokenizer(langBD, "|");

		Boolean contain = false;
		String l ="";
		int c = tokens.countTokens();
		for(int i = 0 ; i < c ; i++)
		{
			l = tokens.nextToken();
			if (l.equals(LANGUAGE_CODE)) {
				contain = true;
			}
		}

		if (contain == false)
			langBD += "|" + LANGUAGE_CODE;

		Editor e = this.getApplicationContext()
				.getSharedPreferences("PiaNinTwitPref", 0).edit();

		e.putString("languagesBD", langBD);
		e.commit();
	}

	public void unzip(String dest) {
		InputStream is;
	     ZipInputStream zis;
	     String zip = dest+LANGUAGE_CODE+".zip";
	     String name = LANGUAGE_CODE+".sqlite";
	     try 
	     {
	         String filename;
	         is = new FileInputStream(zip);
	         zis = new ZipInputStream(new BufferedInputStream(is));          
	         ZipEntry ze;
	         byte[] buffer = new byte[1024];
	         int count;

	         while ((ze = zis.getNextEntry()) != null) 
	         {
	             filename = ze.getName();	           
	             if (ze.isDirectory()) {
	                File fmd = new File(dest + name);
	                fmd.mkdirs();
	                continue;
	             }
	             FileOutputStream fout = new FileOutputStream(dest + name);
	             // cteni zipu a zapis
	             while ((count = zis.read(buffer)) != -1) 
	             {
	                 fout.write(buffer, 0, count);             
	             }
	             fout.close();               
	             zis.closeEntry();
	         }
	         zis.close();
	     } 
	     catch(IOException e)
	     {
	         e.printStackTrace();	        
	     }

	}

	public boolean isOnline() {
		ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo netInfo = cm.getActiveNetworkInfo();
		if (netInfo != null && netInfo.isConnectedOrConnecting()) {
			return true;
		}
		return false;
	}

	public boolean internetConnection() {
		boolean intrntConn = false;
		if (isOnline() == true) {
			intrntConn = true;
		} else {
			intrntConn = false;
		}
		return intrntConn;
	}

}
